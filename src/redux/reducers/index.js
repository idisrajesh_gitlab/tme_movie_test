import { combineReducers } from "redux";
import movies from "./movieReducer";
import loader from "./loadingReducer";
const rootReducer = combineReducers({
  movies,
  loader
});

export default rootReducer;
