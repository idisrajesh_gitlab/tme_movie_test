import * as types from "../actions/actionTypes";

const intialState = {
    isLoading: false
}

const loadingReducer = (state = intialState, action) => {

    if (action.type == types.LOADING_STARTED) {
        return true;
    }
    else if (action.type == types.LOADING_ENDED) {
        return false
    }
    else {
        return intialState;
    }
}

export default loadingReducer;