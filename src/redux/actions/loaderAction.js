import * as types from "./actionTypes";

export const setLoading = () => {
	return {
		type: types.LOADING_STARTED,
	}
}

export const unSetLoading = (data) => {
	return {
		type: types.LOADING_ENDED,
	}
}