import { getRequest } from "../../shared/httpRequest";
import * as types from "./actionTypes";
import { baseurl, apiKey } from "../../constant";

export const searchMovies = async (text, page) => {
    const result = await getSearchMovies(text, page);
    return {
        type: types.SEARCH_MOVIES,
        payload: {
            text,
            searchMovieData: result,
        }
    }
}

export const selectedMovie = (data) => {
    return {
        type: types.SELECT_MOVIES,
        payload: data
    }
}

const getSearchMovies = async (text, page) => {
    const url = `${baseurl}?s=${text?.trimEnd()}&apikey=${apiKey}&page=${page}`;
    const result = await getRequest(url);
    return result.data;
}

// Add movie action functions here as needed.  Use the getRequest function imported above to retrieve
// data from the https://www.omdbapi.com endpoint, either using the apiKey e27098be or creating
// your own.  Refer to https://www.omdbapi.com/ also for API documentations.
