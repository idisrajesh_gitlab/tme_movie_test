// Add action types here as needed.
export const SEARCH_MOVIES = 'SEARCH_MOVIES'
export const SELECT_MOVIES = 'SELECT_MOVIES'
export const LOADING_STARTED = 'LOADING_STARTED'
export const LOADING_ENDED = 'LOADING_ENDED'
