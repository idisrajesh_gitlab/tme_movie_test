import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import './SerachBar.css'

export default function SearchBar({ handleSearch }) {
	const searchText = useSelector(state => state.movies.searchText);
	const [searchQuery, setSearchQuery] = useState(searchText);
	const handlSerachText = (searchText) => {
		setSearchQuery(searchText);
		if (searchText.length >= 4) {
			handleSearch(searchText);

		}
	}
	return (
		<div className='search'>
			<div className='search-Wrapper'>
				<input
					id="search-bar"
					className="search-input"
					onChange={(e) => {
						handlSerachText(e.target.value)
					}}
					value={searchQuery}
					placeholder="Enter a Movie Title..."
				/>
			</div>
		</div>

	)
}
