import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useNavigate } from "react-router";
import { Button } from "@mui/material";
import { baseurl, apiKey } from "../../constant";
import Loader from "../shared/Loader";
import { getRequest } from "../../shared/httpRequest";
import { setLoading, unSetLoading } from "../../redux/actions/loaderAction"

export default function MovieDetailsPage() {
  const dispatch = useDispatch()
  const [selectedMovie, setSelectedMovie] = useState({});
  const { id } = useParams();
  const navigate = useNavigate();
  const getLoaderStatus = () => {
    return useSelector(state => state.loader);
  }
  useEffect(() => {
    const url = `${baseurl}?i=${id}&apikey=${apiKey}`;
    dispatch(setLoading());
    getRequest(url).then(result => {
      setSelectedMovie(result.data)
      dispatch(unSetLoading());
    }).catch(() => dispatch(unSetLoading()))
  }, [])
  return selectedMovie && (getLoaderStatus() === true ? <Loader /> :
    <Card sx={{ display: 'flex', width: "60%", margin: "auto", overflowX: "auto", marginTop: "2rem" }}>
      <Box sx={{ display: 'flex', flexDirection: 'row' }}>
        <CardMedia
          component="img"
          sx={{ width: "24rem", height: "30rem" }}
          image={selectedMovie?.Poster}
          alt={selectedMovie?.title}
        />
        <CardContent sx={{ flex: '1 0 auto' }}>
          <Typography component="div" variant="h4">
            {selectedMovie?.Title} ({selectedMovie?.Year})
          </Typography>
          <Typography variant="h6" color="text.secondary" component="div">
            Director : {selectedMovie.Director}
          </Typography>
          <Typography variant="h6" color="text.secondary" component="div">
            Genre : {selectedMovie.Genre}
          </Typography>
          <Typography variant="h6" color="text.secondary" component="div">
            Language : {selectedMovie.Language}
          </Typography>
          <Typography variant="h6" color="text.secondary" component="div">
            RunTime : {selectedMovie.Runtime}
          </Typography>
          <Typography variant="h6" color="text.secondary" component="div">
            ImdbRating : {selectedMovie.imdbRating}
          </Typography>
          <Button variant="contained" onClick={() => navigate(-1)}>Back</Button>
        </CardContent>
      </Box>

    </Card>)
}
