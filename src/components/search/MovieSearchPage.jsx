import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { searchMovies } from "../../redux/actions/movieActions";
// Data grid to show list of movie search results, but use whatever you like here.
import SearchBar from "../shared/SearchBar";
import Loader from "../shared/Loader";
import MovieList from "./MovieList";
import { setLoading } from "../../redux/actions/loaderAction"

// Text field to allow user to enter search text, but use whatever you like here. 

const MovieSearchPage = (props) => {
  // Use hooks to retrieve list of movies based on user input of search text.  After user enters 4th character in search box,
  // retrieval should happen automatically.
  const getLoaderStatus = () => {
    return useSelector(state => state.loader);
  }
  const [page, setPage] = useState(1);
  const [totalResult, setTotalResult] = useState(0);
  // Render search results here.  Refer to README for specifications.
  const dispatch = useDispatch();
  const getSearchData = (page, searchText) => {
    dispatch(searchMovies(searchText, page));
  }
  const handleSearch = async (searchText) => {
    dispatch(setLoading());
    getSearchData(page, searchText)
  }

  return (
    <>
      <SearchBar handleSearch={handleSearch} />
      {getLoaderStatus() === true ? <Loader /> : <MovieList totalResult={totalResult}
        getSerchData={getSearchData}
        page={page} setPage={setPage} />}
    </>
  )
};

export default MovieSearchPage;
