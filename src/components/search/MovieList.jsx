import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import { useSelector, useDispatch } from 'react-redux';
import "./MovieList.css"
import { useNavigate } from 'react-router';
import Pagination from '@mui/material/Pagination';
import { unSetLoading } from "../../redux/actions/loaderAction"
const Item = styled(Paper)(({ theme }) => ({
	backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
	...theme.typography.body2,
	padding: theme.spacing(1),
	textAlign: 'center',
	cursor: 'pointer',
}));

export default function MovieList({ getSerchData, page, setPage }) {

	const dispatch = useDispatch();
	const movieData = useSelector(state => state.movies.searchMovieData)
	const searchText = useSelector(state => state.movies.searchText);
	const totalResult = movieData && movieData.totalResults
	if (movieData && movieData?.Response != "") {
		dispatch(unSetLoading());
	}
	const onDataPageChange = (event, page) => {
		getSerchData(page, searchText);
		setPage(page);
	}
	return (
		<Box sx={{ width: '70%', marginLeft: '15%' }}>
			<Stack spacing={1}>
				{movieData && movieData.Response == "False" ? <div className='movie-not-found-title'>Movie Not Found</div> :
					<ListItem movieData={movieData} />}
				{movieData.Search?.length > 0 ? <Pagination count={parseInt(totalResult / 10)}
					page={page}
					onChange={onDataPageChange}
					color="primary"
					variant="outlined"
					size="large" /> : <></>}
			</Stack>
		</Box>
	);
}

function ListItem({ movieData }) {
	const navigate = useNavigate();
	const handleClick = (imdbID) => {
		navigate(`details/${imdbID}`)
	}
	return <> {movieData && movieData.Search?.map((item) => <Item onClick={() => handleClick(item.imdbID)}>
		<div className='movie-wrapper'>
			<div>
				<img
					src={item.Poster}
					alt={item.title}
					loading="lazy"
					className='movie-poster' />
			</div>
			<div className='movie-title'>
				<div>
					Title:{item.Title}
				</div>
				<div>
					Year:{item.Year}
				</div>
			</div>
		</div>
	</Item>

	)}
	</>;
}